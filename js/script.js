
var startX = -1;

$(window).load(function() {
   
   
   
   $(window).resize(function(){
      resizer();
   });


   $('.title-razv').click(function(){
      $(this).closest('.razv').toggleClass("active");
   });

   $(window).scroll(function(){
      onScroll();
   });

   $('input[type="text"]').focus(function(){
         var def = $(this).attr("defvalue"),
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('input[type="text"]').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });
   
   onScroll();
   resizer();
   
   $('.close').click(function(){
      $('.form-wind').hide();
   });
   
   $('.foot-but').click(function(ev){
      ev.stopImmediatePropagation();
      $('body').stop().animate({scrollTop:0});
      $('.form-wind').show();
      return false;
   });
   
   $('.openform').click(function(ev){
      ev.stopImmediatePropagation();
      if ($('.adr1').val())
         $('.adr11').val($('.adr1').val());
      if ($('.adr2').val())
         $('.adr22').val($('.adr2').val());
      $('body').stop().animate({scrollTop:0});
      
      $('.form-wind').show();
      return false;
   });
});

function onScroll(){
   var st = $('body').scrollTop()||$(window).scrollTop();
   if (st>123){
      if ($('.footer').attr("f")!=="1") {
         $('.footer').css({
            "position": "fixed",
            "width": "100%",
            "bottom": 0,
            "z-index": "50",
            "opacity": 0,
            "left": 0
         });
         $('body').css({"padding-bottom": "65px"});
         $('.footer').stop().animate({"opacity": 1});
         $('.footer').attr("f", 1);
      }
   } else {
      $('.footer').attr("f",0);
      $('.footer').css({"position":"relative",
         "width":"100%",
         "bottom": 0,
         "left": 0});
      $('body').css({"padding-bottom":"0"});
   }
}

function resizer(){
   $('.form-wind').height("auto");
   setTimeout(function(){
      var w = $('body').width(),
         h = $('body').height();

      $('.form-wind').height(h);
      
   }, 10);
}




//document.onmousemove = mouseMove;

function mouseMove(event){
   event = fixEvent(event);
   var newX = event.pageX - $('.subscribe').offset().left;//-document.body.scrollTop;
   if (startX === -1)
      startX === newX;

   var mar = parseInt((newX -startX)/30,10);

   if (mar){

      $('.subscribe').css("background-position", (mar)+"px 0");

   }
}

function fixEvent(e) {
   // получить объект событие для IE
   e = e || window.event;

   // добавить pageX/pageY для IE
   if ( e.pageX == null && e.clientX != null ) {
      var html = document.documentElement;
      var body = document.body;
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
   }

   // добавить which для IE
   if (!e.which && e.button) {
      e.which = (e.button & 1) ? 1 : ( (e.button & 2) ? 3 : ( (e.button & 4) ? 2 : 0 ) );
   }

   return e;
};



